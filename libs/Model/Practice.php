<?php
namespace Model;

class Practice extends \Emagid\Core\Model {
    static $tablename = "practice";
    public static $fields = [
        'name'=>['required'=>true],
        'original_name',
        'slug'
    ];
}