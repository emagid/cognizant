
function slide( media ) {
    $(media).fadeIn(1000);
    setTimeout(function(){
      $(media).fadeOut(1000);
    }, 6000);
}

function slideStart() {
  
  
  var offset = 0;
  var images = []
  var gifs = []

    images.unshift("http://www.madprops.rocks/slideshow/photo1.jpg", "http://www.hellostranger.com.sg/wp-content/uploads/2013/02/00051.jpg");
    gifs.unshift("https://static.wixstatic.com/media/c11316_9ccc15c96fd04386b552cadf789f303f~mv2.gif", "https://media.giphy.com/media/yoJC2sQXE6yCcDOaVG/giphy.gif");
    


  for ( i=0; i<4; i++) {
    (function(i){
      setTimeout(function(){
          // if even we add pic
          if ( i % 2 === 0 || i === 0 ) {
            var div = "<div class='slide'" + " style='background-image: url(" + images[Math.floor(i/2)].replace('\\','/') + ")'></div>";
            $('#slider').html($('#slider').html()+div);
            slide( $('#slider').children()[$('#slider').children().length -1] );
          }else {
            var div = "<div class='slide'" + " style='background-image: url(" + gifs[Math.floor(i/2)].replace('\\','/') + ")'></div>";
            $('#slider').html($('#slider').html()+div);
            slide( $('#slider').children()[$('#slider').children().length -1] )
          }
      }, offset);
      offset += 6000;
    }(i));  
  }
  setTimeout(function(){

    setTimeout(function(){
      $('#slider .slide:lt(' + 3  +  ')').remove();
    }, 4000);

    images.splice(0,1);
    gifs.splice(0,1);
    slideStart(images, gifs);

  }, offset );
}





function addMedia(images, gifs) {

  for ( i=0; i<images.length; i++ ) {
    var div = "<img class='event_pic' src='" + images[i].replace('\\','/') + "'></div>";
    $('.event_pics .display_images').append(div);
  }
      
  for ( i=0; i<gifs.length; i++ ) {
        var div = "<img class='event_pic' src='" + gifs[i].replace('\\','/') + "'></div>";
      $('.event_pics .display_gifs').append(div);
  }
}

// slideStart();
// addMedia(images, gifs)
