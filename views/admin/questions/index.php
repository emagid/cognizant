<?php
    if(count($model->questions)>0): ?>
  <div class="box box-table">
    <table class="table">
      <thead>
        <tr>
          <th width="25%">Question</th>
          <th width="15%">Image</th>
          <th width="15%">Failure Text</th>
          <th width="15%">Answer</th>
          <th width="15%">Display Order</th>
          <th width="15%" class="text-center">Edit</th>
          <th width="15%" class="text-center">Delete</th>	
        </tr>
      </thead>
      <tbody>
       <?php foreach($model->questions as $obj){ ?>
        <tr>
         <td><a href="<?php echo ADMIN_URL; ?>questions/update/<?= $obj->id ?>"><?php echo $obj->text; ?></a></td>
         <td>
             <a href="<?php echo ADMIN_URL; ?>questions/update/<?= $obj->id ?>">
                 <? if($obj->image != '') { ?>
                     <img src="<?php echo UPLOAD_URL."questions/".$obj->image; ?>" width="50"/>
                 <? } ?>
             </a>
         </td>
         <td><a href="<?php echo ADMIN_URL; ?>questions/update/<?= $obj->id ?>"><?php echo $obj->failure_text; ?></a></td>
         <td>
             <a href="<?php echo ADMIN_URL; ?>questions/update/<?= $obj->id ?>">
                 <?php echo $obj->get_answer(); ?>
             </a>
         </td>
         <td>
             <?php echo $obj->display_order ?>
         </td>
         <td class="text-center">
           <a class="btn-actions" href="<?= ADMIN_URL ?>questions/update/<?= $obj->id ?>">
           <i class="icon-pencil"></i> 
           </a>
         </td>
         <td class="text-center">
           <a class="btn-actions" href="<?= ADMIN_URL ?>questions/delete/<?= $obj->id ?>?token_id=<?php echo get_token();?>" onClick="return confirm('Are You Sure?');">
             <i class="icon-cancel-circled"></i> 
           </a>
         </td>
       </tr>
       <?php } ?>
   </tbody>
 </table>
 <div class="box-footer clearfix">
  <div class='paginationContent'></div>
</div>
</div>
<?php endif; ?>

<?php echo footer(); ?>
<script type="text/javascript">
	var site_url = '<?= ADMIN_URL.'packages';?>';
    var total_pages = <?= $model->pagination->total_pages;?>;
    var page = <?= $model->pagination->current_page_index;?>;
</script>