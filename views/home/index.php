<main>
	<section class="home">

		<header>
			<a href='/'><img src="http://cognizant.com/content/dam/cognizant_foundation/Dotcomimage/COG-Logo-White.svg"></a>
		</header>    

		<body>
			<!--  ==========  PHOTOS  =============== -->
			<section id='photos' class='photos'>
                <h3 class='gif_text'>Scan your QR code below</h3>
				<video id="video" width="1080px" height="1690px" autoplay></video>
                <div id="embed" frameborder="0" allowfullscreen autoplay enablejsapi style="display: none">

                </div>
                <device type="media" onchange="update(this.data)"></device>
                <script>
                    function update(stream) {
                        document.querySelector('video').src = stream.url;
                    }
                </script>

                <div class='qr_message'>
                    <img src="<?=FRONT_ASSETS?>img/qr_code.jpg">
                    <p>Point your QR code at the camera to scan.</p>
                </div>

			</section>

			<canvas id="qr-canvas"style="display:none">
			</canvas>

			<!-- Choosing pictures -->
			<div id='results' style="display:none">

            </div>
            <div id='result' style="display:none">

            </div>

			<div id='qrimg' style="display:none">

			</div>

			<div id='webcamimg' style="display:none">

			</div>

            <div class='slides'>
                <img src='<?=FRONT_ASSETS?>img/slide1.jpg'></img>
                <img src='<?=FRONT_ASSETS?>img/slide2.jpg'></img>
                <img src='<?=FRONT_ASSETS?>img/slide3.jpg'></img>
                <img src='<?=FRONT_ASSETS?>img/slide4.jpg'></img>
            </div>

			<!-- Alerts -->
			<section id='share_alert'>
                <img src="<?=FRONT_ASSETS?>img/check.png">
			</section>


</main>

<!-- images and gifs from the backend -->
<script type="text/javascript" src="<?=FRONT_LIBS?>qr/grid.js"></script>
<script type="text/javascript" src="<?=FRONT_LIBS?>qr/version.js"></script>
<script type="text/javascript" src="<?=FRONT_LIBS?>qr/detector.js"></script>
<script type="text/javascript" src="<?=FRONT_LIBS?>qr/formatinf.js"></script>
<script type="text/javascript" src="<?=FRONT_LIBS?>qr/errorlevel.js"></script>
<script type="text/javascript" src="<?=FRONT_LIBS?>qr/bitmat.js"></script>
<script type="text/javascript" src="<?=FRONT_LIBS?>qr/datablock.js"></script>
<script type="text/javascript" src="<?=FRONT_LIBS?>qr/bmparser.js"></script>
<script type="text/javascript" src="<?=FRONT_LIBS?>qr/datamask.js"></script>
<script type="text/javascript" src="<?=FRONT_LIBS?>qr/rsdecoder.js"></script>
<script type="text/javascript" src="<?=FRONT_LIBS?>qr/gf256poly.js"></script>
<script type="text/javascript" src="<?=FRONT_LIBS?>qr/gf256.js"></script>
<script type="text/javascript" src="<?=FRONT_LIBS?>qr/decoder.js"></script>
<script type="text/javascript" src="<?=FRONT_LIBS?>qr/qrcode.js"></script>
<script type="text/javascript" src="<?=FRONT_LIBS?>qr/findpat.js"></script>
<script type="text/javascript" src="<?=FRONT_LIBS?>qr/alignpat.js"></script>
<script type="text/javascript" src="<?=FRONT_LIBS?>qr/databr.js"></script>
<script type="text/javascript" src="<?=FRONT_LIBS?>qr/instascan.min.js"></script>
<script type="text/javascript">

$(document).ready(function(){

    $('.slides').slick({
                    slidesToShow: 1,
                    autoplay: true,
                    autoplaySpeed: 7000
            });
});



    var tag = document.createElement('script');

    tag.src = "https://www.youtube.com/iframe_api";
    var firstScriptTag = document.getElementsByTagName('script')[0];
    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);



    var player;
    function onYouTubeIframeAPIReady() {
        player = new YT.Player('embed', {
            playerVars: {autoplay:1},
            events: {
                'onStateChange': reload
            }
        });
    }
    function reload(event) {
        if(event.data === 0) {
            $('#embed').fadeOut();
            $('.slides').fadeIn();
        }
    }
    function getId(url) {
        var regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/;
        var match = url.match(regExp);

        if (match && match[2].length == 11) {
            return match[2];
        } else {
            return 'error';
        }
    }
    let scanner = new Instascan.Scanner({ video: document.getElementById('preview') });
    scanner.addListener('scan', function (a) {
        console.log(a);
        if(a.indexOf("http://") === 0 || a.indexOf("https://") === 0) {
            var yid = getId(a);

            console.log(yid);
            $('#share_alert').fadeIn();
            $('#share_alert').css('display', 'flex');

            setTimeout(function(){
                $('.slides').fadeOut();
                $('#share_alert').fadeOut();
                $('#embed').fadeIn();
                player.loadVideoById(yid);
            }, 2000);
        }
    });
    Instascan.Camera.getCameras().then(function (cameras) {
        if (cameras.length > 0) {
            scanner.start(cameras[0]);
        } else {
            console.error('No cameras found.');
        }
    }).catch(function (e) {
        console.error(e);
    });



</script>


