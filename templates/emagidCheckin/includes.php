
<link rel = "stylesheet" type = "text/css" href = "<?=FRONT_CSS?>main.css">
<link rel = "stylesheet" type = "text/css" href = "<?=FRONT_CSS?>footer.css">
<link rel = "stylesheet" type = "text/css" href = "<?=FRONT_CSS?>material.css">
<link rel = "stylesheet" type = "text/css" href = "<?=FRONT_CSS?>nouislider.css">
<link rel = "stylesheet" type = "text/css" href = "<?=FRONT_LIBS?>bxslider/jquery.bxslider.css">
<link rel = "stylesheet" href = "https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
<link rel = "shortcut icon" href = "<?=FRONT_ASSETS?>img/american-favicon.png"> 

<script src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
<script src="https://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
<script src="<?=FRONT_JS?>main.js"></script>
<script src="<?=FRONT_JS?>slider.js"></script>
<script src="<?=FRONT_JS?>camera.js"></script>
<script src="<?=FRONT_JS?>material.js"></script>
<script src="<?=FRONT_JS?>nouislider.js"></script>
<script src="<?=FRONT_JS?>js.cookie.js"></script>
<script src="<?=FRONT_JS?>isotope.min.js"></script>
<script src="<?=FRONT_JS?>velocity.js"></script>
<script src="<?=FRONT_JS?>wNumb.js"></script>
<script src="<?=FRONT_LIBS?>/iscroll/iscroll.js"></script>
<script src="<?=FRONT_LIBS?>jquery.gdocsviewer.js"></script>
<script src="<?=FRONT_LIBS?>jquery.simplesidebar.js"></script>
<script src="<?=FRONT_LIBS?>jquery.zoom.js"></script>
<script type="text/javascript" src="<?=FRONT_LIBS?>slick/slick/slick.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?=FRONT_LIBS?>slick/slick/slick.css"/>
<link rel="stylesheet" type="text/css" href="<?=FRONT_LIBS?>slick/slick/slick-theme.css"/>

<link rel="stylesheet" href="<?=FRONT_LIBS?>scrollbar/jquery.mCustomScrollbar.css" />
<script src="<?=FRONT_LIBS?>scrollbar/jquery.mCustomScrollbar.concat.min.js"></script>

<script src="<?=FRONT_LIBS?>slidesjs/jquery.slides.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js" integrity="sha512-K1qjQ+NcF2TYO/eI3M6v8EiNYZfA95pQumfvcVrTHtwQVDG+aHRqLi/ETn2uB+1JqwYqVG3LIvdm9lj6imS/pQ==" crossorigin="anonymous"></script>

<? if ($this->emagid->route['controller'] == 'category') { ?>
    <link rel = "stylesheet" type = "text/css" href = "<?=FRONT_CSS?>categories.css">
    <script src="<?=FRONT_JS?>category.js"></script>
<? } ?>

<? if ($this->emagid->route['controller'] == 'checkout') { ?>
    <link rel = "stylesheet" type = "text/css" href = "<?=FRONT_CSS?>checkout.css">
    <script src="<?=FRONT_JS?>checkout.js"></script>
<? } ?>

<? if ($this->emagid->route['controller'] == 'cart') { ?>
    <link rel = "stylesheet" type = "text/css" href = "<?=FRONT_CSS?>checkout.css">
    <script src="<?=FRONT_JS?>checkout.js"></script>
<? } ?>

<? if ($this->emagid->route['controller'] == 'account') { ?>
    <link rel = "stylesheet" type = "text/css" href = "<?=FRONT_CSS?>account.css">
<? } ?>

<? if ($this->emagid->route['controller'] == 'wishlist') { ?>
    <link rel = "stylesheet" type = "text/css" href = "<?=FRONT_CSS?>wishlist.css">
<? } ?>

<? if ($this->emagid->route['controller'] == 'expertise') { ?>

    <link rel = "stylesheet" type = "text/css" href = "<?=FRONT_CSS?>expertise.css">
<? } ?>

<? if ($this->emagid->route['controller'] == 'wholesale') { ?>
    <link rel = "stylesheet" type = "text/css" href = "<?=FRONT_CSS?>wholesale.css">
    <script src="<?=FRONT_LIBS?>jquery.maskedinput.js" type="text/javascript"></script>
<? } ?>

<? if ($this->emagid->route['controller'] == 'contact') { ?>
    <link rel = "stylesheet" type = "text/css" href = "<?=FRONT_CSS?>contact.css">
<? } ?>

<? if ($this->emagid->route['controller']== 'inquiry') { ?>
    <link rel = "stylesheet" type = "text/css" href = "<?=FRONT_CSS?>inquiry.css">
    <script type="text/javascript" src="/content/admin/js/plugins/jquery-validation/dist/jquery.validate.min.js"></script>
<? } ?>

<? if ($this->emagid->route['controller'] == 'about') { ?>
    <link rel = "stylesheet" type = "text/css" href = "<?=FRONT_CSS?>about.css">
<? } ?>

<? if ($this->emagid->route['controller'] == 'emagidCheckin') { ?>
    <link rel = "stylesheet" type = "text/css" href = "<?=FRONT_CSS?>wholesale.css">
    <link rel = "stylesheet" type = "text/css" href = "<?=FRONT_CSS?>productinfo.css">
<? } ?>

<? if ($this->emagid->route['controller'] == 'pages') { ?>
    <link rel = "stylesheet" type = "text/css" href = "<?=FRONT_CSS?>shippingReturns.css">
     <link rel = "stylesheet" type = "text/css" href = "<?=FRONT_CSS?>expertise.css">
<? } ?>

<? if ($this->emagid->route['controller'] == 'cart') { ?>
    <link rel = "stylesheet" type = "text/css" href = "<?=FRONT_CSS?>checkout.css">
    <script src="<?=FRONT_JS?>checkout.js"></script>
<? } ?>

<? if ($this->emagid->route['controller'] == 'browse') { ?>
    <link rel = "stylesheet" type = "text/css" href = "<?=FRONT_CSS?>browse_rings.css">
    <link rel = "stylesheet" type = "text/css" href = "<?=FRONT_CSS?>search_create.css">
    <link rel = "stylesheet" type = "text/css" href = "<?=FRONT_CSS?>categories.css">
    <script src="<?=FRONT_JS?>category.js"></script>
    <link rel = "stylesheet" type = "text/css" href = "<?=FRONT_CSS?>product.css">
    <script src="<?=FRONT_JS?>product.js"></script>
<? } ?>

<? if ($this->emagid->route['controller'] == 'diamonds') { ?>
    <link rel = "stylesheet" type = "text/css" href = "<?=FRONT_CSS?>search_create.css">
    <link rel = "stylesheet" type = "text/css" href = "<?=FRONT_CSS?>browse_rings.css">
    <script src="<?=FRONT_JS?>search_create.js"></script>
<? } ?>


<? if ($this->emagid->route['controller'] == 'products' || $this->emagid->route['controller'] == 'weddingbands' || $this->emagid->route['controller'] == 'rings' || $this->emagid->route['controller'] == 'jewelry') { ?>
    <link rel = "stylesheet" type = "text/css" href = "<?=FRONT_CSS?>categories.css">
    <script src="<?=FRONT_JS?>category.js"></script>
    <link rel = "stylesheet" type = "text/css" href = "<?=FRONT_CSS?>product.css">
    <script src="<?=FRONT_JS?>product.js"></script>
<? } ?>